function load () {
    let req = new XMLHttpRequest();
    req.open("GET", "https://swapi.dev/api/films/");
    req.onload = function() {
        if (req.status === 200) {
            let movies;
            movies = JSON.parse(req.responseText);
            movies.results.forEach(function(elem){
                let title = document.createElement('h2');
                title.innerText = elem.title;
                document.body.append(title);

                let crawls = document.createElement('h3');
                crawls.innerText = `Opening crawl: ${elem.opening_crawl}`;
                title.after(crawls);

                let episode = document.createElement('h3');
                episode.innerText = `Episode: ${elem.episode_id}`;
                title.after(episode);
                elem.characters.forEach(function(elem){
                    let req = new XMLHttpRequest();
                    req.open("GET", elem);
                    req.onload = function() {
                        let chars;
                        chars = JSON.parse(req.responseText);
                        let para = document.createElement('p');
                        para.innerText = chars.name;
                        title.after(para);
                        spinnerOff ()
                    }

                    req.send();
                })
            })
        }
    };
    req.onprogress = function () {
        spinner()
    };
    req.send();
}

load();


function spinner () {
    let spinner = document.querySelector('.spinner');
    spinner.classList.add('spin');
}

function spinnerOff () {
    let spinner = document.querySelector('.spinner');
    spinner.classList.remove('spin');
}